<?php
//indexed array
$cars=array("Volvo","BMW","Toyota");
echo "<pre>";
print_r($cars);
echo "</pre>";
echo "I like". $cars[0]." , ".$cars[1]." and ".$cars[2];
?>
<br>
<?php
//associative array
$age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
echo "<pre>";
print_r($age);
echo "</pre>";
echo "Peter is ".$age['Peter']." years old.";
echo "<br>";

foreach($age as $key=>$value){
    echo "Key:".$key." Value:".$value."<br>";
}
?>

