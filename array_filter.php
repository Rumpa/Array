<?php
//array_filter() without callback
$entry=array(
    0=>"foo",
    1=>false,
    2=>-1,
    3=>null,
    4=>""
);
echo "<pre>";
print_r(array_filter($entry));
echo "</pre>";
?>
<br>

<?php
//array_filter() with callback
function odd($var){
    return($var & 1);

}
function even($var){
    return(!($var & 1));
}
$array1=array(1,2,3,4,5,6,7,8,9,10);
echo "Odd number:<br>";
print_r(array_filter($array1,"odd"));
echo "<br>Even number:";
echo "<br>";
print_r(array_filter($array1,"even"));
?>
