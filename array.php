<?php
$array=array(1,2,3,4,5);
echo "<pre>";
print_r($array);
echo "</pre>";

// Now delete every item, but leave the array itself intact:
foreach($array as $key=>$value){
    unset($array[$key]);
}
print_r($array);
echo "<br>";

// Append an item (note that the new key is 5, instead of 0).

$array[]=6;
print_r($array);

// Re-index:
$array=array_values($array);
$array[]=7;
echo "<pre>";
print_r($array);
echo "</pre>";
?>